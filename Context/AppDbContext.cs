﻿using ElasticProject.Models;
using Microsoft.EntityFrameworkCore;

namespace ElasticProject.Context
{
	public class AppDbContext : DbContext
	{
		public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
		{

		}

		public DbSet<Product> Products { get; set; }
	}
}
