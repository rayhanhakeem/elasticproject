﻿namespace ElasticProject.Models
{
	public class GetProduct
	{
		public string Title { get; set; }
		public string Description { get; set; }
		public int Price { get; set; }
		public int Quantity { get; set; }
	}
}
