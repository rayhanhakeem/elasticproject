﻿using ElasticProject.Context;
using ElasticProject.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Nest;
using System.Globalization;

namespace ElasticProject.Controllers
{
	[ApiController]
	[Route("[controller]")]
	public class ProductsController : ControllerBase
	{
		private readonly IElasticClient _elasticClient;
		private readonly ILogger<ProductsController> _logger;
		private readonly AppDbContext _context;

		public ProductsController(ILogger<ProductsController> logger,IElasticClient elasticClient, AppDbContext appDbContext)
		{
			_logger = logger;
			_elasticClient = elasticClient;
			_context = appDbContext;
		}

		[HttpGet(Name = "GetProducts")]
		public async Task<IActionResult> Get(string keyword)
		{
			var result = await _elasticClient.SearchAsync<Product>(
				s => s.Query(q =>
					q.QueryString(d =>
						d.Query('*' + keyword + '*')
						)
					).Size(1000)
				);

			return Ok(result.Documents.ToList());
		}

		[HttpPost(Name = "PostProducts")]
		public async Task<IActionResult> Post(Product product)
		{
			//karena id masih manual jadi cek dulu apakah idnya udah ada di db
			var idExist = await _context.Products.Where(q => q.Id == product.Id).FirstOrDefaultAsync();
			if(idExist != null) { return BadRequest("Id already exist!"); }
			//check title produk apakah udah ada di db
			var titleExist = await _context.Products.Where(q => q.Title == product.Title).FirstOrDefaultAsync();
			if(titleExist != null) { return BadRequest("Title already exist!"); }
			_context.Products.Add(product);
			await _context.SaveChangesAsync();
			await _elasticClient.IndexDocumentAsync(product);
			return Ok();
		}

		[HttpPut(Name = "UpdateProducts")]
		public async Task<IActionResult> Update(string indexName, Product product, int id)
		{
			//check dulu apakah idnya ada di db
			var idExist = await _context.Products.Where(q=>q.Id== id).FirstOrDefaultAsync();
			if (idExist == null)
			{
				return BadRequest("Id not found!");
			}
			idExist.Title = product.Title;
			idExist.Description = product.Description;
			idExist.Price = product.Price;
			idExist.Quantity = product.Quantity;
			_context.Products.Update(idExist);
			var response = await _elasticClient.CreateAsync(product, q => q.Index(indexName));
			if(response.ApiCall?.HttpStatusCode == 409)
			{
				await _elasticClient.UpdateAsync<Product>(product.Id,p=>p.Index(indexName).Doc(product));
			}
			await _context.SaveChangesAsync();
			return Ok();
		}
		[HttpDelete(Name = "DeleteProduct")]
		public async Task<IActionResult> Delete(string indexName, Product product)
		{
			var response = await _elasticClient.CreateAsync(product,q=>q.Index(indexName));
			if(response.ApiCall?.HttpStatusCode== 409)
			{
				await _elasticClient.DeleteAsync(DocumentPath<Product>.Id(product.Id).Index(indexName));
			}
			return Ok();
		}
	}
}
